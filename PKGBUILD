# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=pacman
pkgver=6.0.1
pkgrel=3
pkgdesc="A library-based package manager with dependency support"
arch=('x86_64')
url="https://www.archlinux.org/pacman/"
license=('GPL')
groups=('base-devel')
depends=('bash' 'glibc' 'libarchive' 'curl' 'gpgme'
    'gettext' 'gawk' 'coreutils' 'gnupg' 'grep')
makedepends=('meson' 'asciidoc' 'doxygen')
checkdepends=('python' 'fakechroot')
options=('strip')
backup=(etc/pacman.conf
    etc/makepkg.conf)
source=(https://sources.archlinux.org/other/${pkgname}/${pkgname}-${pkgver}.tar.xz
    add-flto-to-LDFLAGS-for-clang.patch
    fix-wkd-lookup.patch
    libmakepkg-add-extra-buildflags-only-when-buildflags.patch
    make-link-time-optimization-flags-configurable.patch
    makepkg.conf
    makepkg-use-ffile-prefix-map-instead-of-fdebug-prefi.patch
    mirrorlist
    pacman.conf)
sha256sums=(0db61456e56aa49e260e891c0b025be210319e62b15521f29d3e93b00d3bf731
    82ff91b85f4c6ceba19f9330437e2a22aabc966c2b9e2a20a53857f98a42c223
    8ab5b1338874d7d58e11c5d1185ea3454fcc89755f9c18faf87ff348ad1ed16c
    7d0aee976c9c71fcf7c96ef1d99aa76efe47d8c1f4451842d6d159ec7deb4278
    5b43e26a76be3ed10a69d4bfb2be48db8cce359baf46583411c7f124737ebe6a
    b822aac17fc18a6bf0fc904f3b643b37af61c9680f847af3ac0af0c6093b4cfd
    b940e6c0c05a185dce1dbb9da0dcbebf742fca7a63f3e3308d49205afe5a6582
    cd0507a2aa0475815e1fcae8702e424ee22e94ab1e499372fb9449ca2e5471b8
    81133a98f09a59c89a6a4228924b2c8121f870596f3b9c7f0dda030741edf616)


prepare() {
    cd ${pkgname}-${pkgver}

    patch -Np1 -i ${srcdir}/add-flto-to-LDFLAGS-for-clang.patch
    patch -Np1 -i ${srcdir}/makepkg-use-ffile-prefix-map-instead-of-fdebug-prefi.patch
    patch -Np1 -i ${srcdir}/libmakepkg-add-extra-buildflags-only-when-buildflags.patch
    patch -Np1 -i ${srcdir}/make-link-time-optimization-flags-configurable.patch
    patch -Np1 -i ${srcdir}/fix-wkd-lookup.patch
}
build() {
    cd ${pkgname}-${pkgver}

    meson --prefix=/usr                         \
        --sysconfdir=/etc                       \
        --buildtype=release                     \
        -Ddoc=enabled                           \
        -Ddoxygen=enabled                       \
        -Dscriptlet-shell=/usr/bin/bash         \
        -Dldconfig=/usr/sbin/ldconfig           \
        --pkg-config-path=/usr/lib64/pkgconfig  \
        build

    meson compile -C build
}

package() {
    cd ${pkgname}-${pkgver}

    DESTDIR=${pkgdir} meson install -C build

    install -vDm644 ${srcdir}/makepkg.conf ${pkgdir}/etc/makepkg.conf
    install -vDm644 ${srcdir}/pacman.conf  ${pkgdir}/etc/pacman.conf
    install -vDm644 ${srcdir}/mirrorlist ${pkgdir}/etc/pacman.d/mirrorlist
}
